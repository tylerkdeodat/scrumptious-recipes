from django.urls import path

from meal_plan.views import (
    MealPlanListView,
    MealPlanCreateView,
    MealPlanDetailView,
    MealPlanUpdateView,
    MealPlanDeleteView,
)


urlpatterns = [
    path("", MealPlanListView.as_view(), name="mealplan_list"),
    path(
        "create/",
        MealPlanCreateView.as_view(),
        name="meal_plan_create",
    ),
    path(
        "<int:pk>/",
        MealPlanDetailView.as_view(),
        name="meal_detail",
    ),
    path(
        "<int:pk>/edit/",
        MealPlanUpdateView.as_view(),
        name="meal_edit",
    ),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_delete",
    ),
]
