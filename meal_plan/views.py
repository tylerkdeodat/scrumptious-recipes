from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from meal_plan.models import MealPlan
from django.contrib.auth.mixins import LoginRequiredMixin

from recipes.models import Recipe


class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 3

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    success_url = reverse_lazy("mealplan_list")
    fields = ["name", "date", "recipes"]

    def form_valid(self, form):
        form.instance.author = self.request.user
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        plan.save_m2m()
        return redirect("meal_detail", pk=plan.id) and super().form_valid(form)


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name", "description"]
    success_url = reverse_lazy("mealplan_list")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

    def get_success_url(self):
        return reverse_lazy("meal_plan_detail", args=[self.object.id])


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("mealplan_list")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)
